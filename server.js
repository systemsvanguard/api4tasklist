const jsonServer = require('json-server');
// cors NOT enabled. 
const server = jsonServer.create();
const router = jsonServer.router('tasklistdb.json');
const middlewares = jsonServer.defaults();
const port = process.env.PORT || 4000;

server.use(middlewares);

server.use(router);

server.listen(port, () => {
    console.log(`Numeris RESTful API web application now running on port ${port}. Enjoy!`);
});
// server.listen(port);  

